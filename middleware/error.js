const errorResponse = require('../utils/errResponse')

const errorHandler = (err, req, res, next) => {
    let errorVal = { ...err };

    // Log to console for dev
    console.log(err, err.name);

    // Mongoose bad ObjectId
    errorsMsg = {
        CastError: err.message,
        MongoServerError: err.message
    };

    errorVal = new errorResponse(errorsMsg[errorsMsg.name], 404);
    res.status(errorVal.statusCode || 500).json({ success: false, error: err.message || 'Internal Server Error'});
};

    module.exports = errorHandler;