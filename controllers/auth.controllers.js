const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errResponse');
const sendEmail = require('../utils/sendEmail.utils')
const User = require('../models/user.mongo');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const {
    getSignedJwtToken,
    sendTokenResponse,
    getResetPasswordToken
} = require('../utils/tokenHelpers.utils')

// --- Helpers

const matchPassword = async (password, userModelPassword) => await bcrypt.compare(password, userModelPassword.password);

// --- End Helpers


// @desc        Register user
// @route       POST /api/v1/auth/register
// @access      Public
exports.register = asyncHandler( async (req, res, next) => {
    const { name, email, password, role } = req.body;

    // Check user name and login for duplicates
    const isPresent = await User.find({ email: email })

    if (isPresent.length > 0) {
        return next( new ErrorResponse('User with such email already exist', 400));
    }

    // Create user
    const user = await User.create({
        name,
        email,
        password,
        role,
    });

    // Create token and send it
    sendTokenResponse(user, 200, res);
});


// @desc        Update user
// @route       PUT /api/v1/users/:id
// @access      Private
exports.updateUser = asyncHandler( async (req, res, next) => {
    const { name, email, password, role } = req.body;

    // Check user name and login for duplicates
    const isPresent = await User.findOne({ email: email })

    if (isPresent.length < 0) {
        return next( new ErrorResponse(`No such user ${email}`, 400));
    }

    // Update user
    const user = await User.findByIdAndUpdate(req.user.id, {
        name,
        email,
        password,
        role,
    });

    res.status(200).json({
        success: true,
        data: user
    })

});


// @desc        Login user
// @route       POST /api/v1/auth/login
// @access      Private
exports.login = asyncHandler( async (req, res, next) => {
    const { email, password } = req.body;

    // Validate email and password
    if (!email || !password) {
        return next( new ErrorResponse('Please provide an email and password', 400));
    }

    // Check for user
    const user = await User.findOne({ email: email })
        .select('+password');

    if ( user.email.length === 0 ) {
        return next( new ErrorResponse('No such user...', 400) );
    }

    // Check if password
    // const isMatch = await user.matchPassword(password);
    const isMatch = await bcrypt.compare(password, user.password)


    if (!isMatch) {
        return next( new ErrorResponse('Invalid credentials', 400));
    }

    sendTokenResponse(user, 200, res);
    // res.status(200).json({ success: true, token: token() })
});


// @desc        Get current logged in user
// @route       GET /api/v1/auth/me
// @access      Private
exports.getMe = asyncHandler( async (req, res, next) => {
    // "user" -- уже будет в "req" (req.user) после вызова "protected"
    // в "routes"
    // const user = await User.find({ email: req.body.email });

    res.status(200).json({
        success: true,
        data: req.user
    })
});


// @desc        Logout
// @route       GET /api/v1/auth/logout
// @access      Private
exports.getLogout = asyncHandler( async (req, res, next) => {
    // "user" -- уже будет в "req" (req.user) после вызова "protected"
    // в "routes"
    // const user = await User.find({ email: req.body.email });
    res.cookie('token', 'none', {
        expires: new Date(Date.now() + 10 * 1000),
        httpOnly: true  // обеспечивает отправку cookie только с использованием протокола HTTP(S)
    })


    res.status(200).json({
        success: true,
        data: {}
    })
});


// @desc        Forgot the password
// @route       POST /api/v1/auth/forgotpassword
// @access      Public
exports.forgotPassword = asyncHandler( async (req, res, next) => {
    // "user" -- уже будет в "req" (req.user) после вызова "protected"
    // в "routes"
    const user = await User.findOne({ email: req.body.email });

    if (!user) {
        return next(
            new ErrorResponse( `No such user with that email: ${req.body.email}`, 404 )
        );
    };

    const resetToken = getResetPasswordToken(user);

    await user.save({ validateBeforeSave: false });

    // Create reset url
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/auth/resetpassword/${resetToken}`;

    const message = `<p>This email because reset password request</p> <a href="${resetUrl}">Reset link</a> `;

    try {
        await sendEmail({
            email: user.email,
            subject: 'Password reset token',
            message
        })
        res.status(200).json({
            success: true,
            data: 'Email sent'
        })
    } catch (e) {
        console.log(e)
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;
        await user.save();
        return next(
            new ErrorResponse(
                'Email could not be sent', 500
            )
        );
    }

    res.status(200).json({
        success: true,
        data: user
    })
});


// @desc        Reset password
// @route       PUT /api/v1/auth/resetpassword/:resettoken
// @access      Public
exports.resetPassword = asyncHandler( async (req, res, next) => {
    const resetPasswordToken = crypto
        .createHash('sha256')
        .update(req.params.resettoken)
        .digest('hex');

    const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: { $gt: Date.now() }
    });

    if (!user) {
        return next(
            new ErrorResponse('Invalid token', 400)
        )
    };

    // Set new password
    user.password = req.body.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;
    await user.save();

    // Create token and send it
    sendTokenResponse(user, 200, res);
});


// @desc        Update user details
// @route       PUT /api/v1/auth/updatedetails
// @access      Private
exports.updateDetails = asyncHandler( async (req, res, next) => {
    // "user" -- уже будет в "req" (req.user) после вызова "protected"
    // в "routes"
    // const user = await User.find({ email: req.body.email });

    const fieldsToUpdate = {
        name: req.body.name,
        email: req.body.email
    };

    const user = await User.findByIdAndUpdate(req.user.id, fieldsToUpdate, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: user
    })
});


// @desc        Update password
// @route       POST /api/v1/auth/updatepassword
// @access      Private
exports.updatePassword = asyncHandler( async (req, res, next) => {
    // "user" -- уже будет в "req" (req.user) после вызова "protected"
    // в "routes"

    /** .select() -- Specifies which document fields to include (+) or exclude (-) */
    const user = await User.findById(req.user.id).select('+password');

    // Check current password
    if (!(await matchPassword(req.body.currentPassword, user))) {
        return next( new ErrorResponse('Password is incorrect', 401))
    }

    user.password = req.body.newPassword;
    await user.save();

    sendTokenResponse(user, 200, res);
});