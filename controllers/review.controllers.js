const asyncHandler = require('../middleware/async');
const Review = require('../models/review.mongo');
const devCampModel = require('../models/bootcamp.mongo');
const ErrorResponse = require('../utils/errResponse');


// @desc        Get reviews
// @route       GET /api/v1/reviews
// @route       GET /api/v1/bootcamps/:bootcampId/reviews
// @access      Public
exports.getReviews = asyncHandler( async (req, res, next) => {
    if(req.params.bootcampId) {
        const reviews = await Review.find({ bootcamp: req.params.bootcampId });
        return res.status(200).json({
            success: true,
            count: reviews.length,
            data: reviews
        })
    } else {
        return res.status(200).json(res.advancedResults)
    };

});


// @desc        Get single review
// @route       GET /api/v1/reviews/:id
// @access      Public
exports.getSingleReview = asyncHandler( async (req, res, next) => {
    const review = await Review.findById(req.params.id).populate({
        path: 'bootcamp',
        select: 'name description'
    })

    if (!review) {
        return next( new ErrorResponse(`No review found with the id of: ${req.params.id}`), 404)
    }

    res.status(200).json({
        success: true,
        data: review
    })

});


// @desc        Add review
// @route       POST /api/v1/bootcamps/:bootcampId/reviews
// @access      Private
exports.addReview = asyncHandler( async (req, res, next) => {
    req.body.bootcamp = req.params.bootcampId;
    req.body.user = req.user.id;

    const bootcamp = await devCampModel.findById(req.body.bootcamp);

    if (!bootcamp) {
        return next( new ErrorResponse(`No bootcamp with the id of ${req.params.bootcampId}`, 404))
    };

    const review = await Review.create(req.body);

    res.status(201).json({
        success: true,
        data: review
    })

});


// @desc        Update review
// @route       PUT /api/v1/reviews/:id
// @access      Private
exports.updReview = asyncHandler( async (req, res, next) => {
    let updateReview = await Review.findById(req.params.id);

    if (!updateReview) {
        return next(
            new ErrorResponse(`No review with such id - ${req.params.id}`)
        )
    };

    // Make sure user is owner of review or admin
    if (updateReview.user.toString() !== req.user.id && req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User id is not owner of this review: ${updateReview.title}`, 404)
        );
    };

    updateReview = await Review.findOneAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: updateReview
    })
});


// @desc        Delete review
// @route       DELETE /api/v1/reviews/:id
// @access      Private
exports.deleteReview = asyncHandler( async (req, res, next) => {
    const reviewDelete = await Review.findById(req.params.id);

    if (!reviewDelete) {
        return next(
            new ErrorResponse(`No course with such id: ${req.params.id}`)
        )
    };

    // Make sure the user is owner of review
    if (reviewDelete.user.toString() !== req.user.id && req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User id is not owner of that review: ${reviewDelete._id}`, 404)
        );
    };

    await reviewDelete.remove();

    res.status(200).json({
        success: true,
        data: {}
    })
});