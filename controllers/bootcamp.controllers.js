const path = require('path');
const asyncHandler = require('../middleware/async');
const geocoder = require('../utils/geocoder');
const devCampModel = require('../models/bootcamp.mongo');
const ErrorResponse = require('../utils/errResponse');

// @desc        Get all bootcamps
// @route       GET /api/v1/bootcamp
// @access      Public
exports.getBootcamps = asyncHandler( async (req, res, next) => {
    // Executing query
    res.status(200).json(res.advancedResults);

    // next(e);
    // next(new errorResponse(`Bootcamp not found with: ${req.body}`, 404));
    // res.status(400).json({
    //     success: false
    // })
    }
);

// @desc        Get single bootcamp
// @route       GET /api/v1/bootcamp/:id
// @access      Public
exports.getBootcamp = asyncHandler( (async (req, res, next) => {
        const record = await devCampModel.findById(req.params.id)
        res.status(200).json({ success: true, data: record})
        // next(e);
        // next(new errorResponse(`Bootcamp not found with: ${req.params.id}`, 404));
        // res.status(400).json({ success: false, data: e.message})
    })
);

// @desc        Create new bootcamp
// @route       POST /api/v1/bootcamp/
// @access      Private
exports.createBootcamp = asyncHandler( (async (req, res, next) => {
        // Add user to req.body
        req.body.user = req.user.id;

        // Check for publish bootcamp
        const publisherBootcamp = await devCampModel.findOne({ user: req.body.user });

        // If user in NOT admin, only ONE bootcamp per user
        if (publisherBootcamp && req.user.role !== 'admin') {
            return next( new ErrorResponse(`The user with ID ${req.user.id} has already published a bootcamp`, 400));
        };

        const campModel = await devCampModel.create(req.body);

        res.status(201).json({
            success: true,
            data: campModel
        });
        // next(e);
        // res.status(400).json({
        //     success: false
        // });

    })
);

// @desc        Update bootcamp
// @route       PUT /api/v1/bootcamp/:id
// @access      Private
exports.updateBootcamp = asyncHandler( (async (req, res, next) => {
        let updatedRecord = await devCampModel.findById(req.params.id);

        if (!updatedRecord) {
            return next(
                new ErrorResponse(`No such course with such id: ${req.params.id}`, 404)
            );
        };

        // Make sure is owner of course
        if (updatedRecord.user.toString() !== req.user.id && req.user.role !== 'admin') {
            return next(
                new ErrorResponse(`User id is not owner of that bootcamp: ${req.params.id}`, 404)
            );
        };

        updatedRecord = await devCampModel.findOneAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        res.status(200).json({ status: true, data: updatedRecord });
        // next(e);
        // next(new errorResponse(`Bootcamp error ${e.message} with: ${req.params.id}`, 404));
        // res.status(400).json({ success: false, data: e.message});
    })
);

// @desc        Delete bootcamp
// @route       DELETE /api/v1/bootcamp/:id
// @access      Public
exports.deleteBootcamp = asyncHandler( (async (req, res, next) => {
        const bootcamp = await devCampModel.findById(req.params.id);

        if (!bootcamp) {
            return next(
                new ErrorResponse(`Bootcamp not found by ${req.params.id}`, 404)
            );
        }

        // Make sure is owner of course
        if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
            return next(
                new ErrorResponse(`User id is not owner of that bootcamp: ${req.params.id}`, 404)
            );
        };

        bootcamp.remove();

        res.status(200).json({
            status: true,
            data: null
        });

        // next(e);
        // res.status(400).json({ success: false, data: e.message});
    })
);

// @desc        Get bootcamp within radius
// @route       DELETE /api/v1/bootcamp/radius/:zipcode/:distance
// @access      Private
exports.getBootcampInRadius = asyncHandler( async (req, res, next) => {
    const { zipcode, distance } = req.params;

    // Get lat/lng from geocoder
    const loc = await geocoder.geocode(zipcode);
    const lat = loc[0].latitude;
    const lng = loc[0].longitude;

    // Calc radius using radiant
    // Divide dist by radius of Earth
    // Earth r = 3,963 / 6,378 km
    const radius = distance / 6378

    const bootcamps = await devCampModel.find({
        location: { $geoWithin: { $centerSphere: [ [lng, lat], radius]}}
    })

    res.status(200).json({
        success: true,
        count: bootcamps.length,
        data: bootcamps
    });

    }
);

// @desc        Upload photo bootcamp
// @route       PUT /api/v1/bootcamp/:id/photo
// @access      Private
exports.uplPhotoBootcamp = asyncHandler( (async (req, res, next) => {

    const bootcamp = await devCampModel.findById(req.params.id);

    if (!bootcamp) {
        return next(
            new ErrorResponse(`Bootcamp not found by ${req.params.id}`, 404)
        );
    };

    if (!req.files) {
        return next(
            new ErrorResponse('Please upload the file', 400)
        )
    };

    const file = req.files.file;

    // Make sure that file is pic
    if (!file.mimetype.startsWith('image')) {
        return next(
            new ErrorResponse('Please upload an image file', 400)
        )
    };

    // Make sure is owner of course
    if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
        return next(
            new ErrorResponse(`User id is not owner of that bootcamp: ${req.params.id}`, 404)
        );
    };

    // Block oversize pics
    if (file.size > process.env.MAX_UPL_SIZE) {
        return next(
            new ErrorResponse(`Max size of pic is ${process.env.MAX_UPL_SIZE}`, 400)
        )
    };

    // Create uniq name for photo
    file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
            console.error(err);
            return new ErrorResponse('Problem with file upload', 400)
        }

        await devCampModel.findOneAndUpdate(req.params.id, { photo: file.name})
    })

    console.log(file)

    res.status(200).json({
        status: true,
        data: file.name
    });

    // next(e);
    // res.status(400).json({ success: false, data: e.message});
    })
);