const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

// Load ENV
dotenv.config({
    path: './config/config.env'
});

// Load models
const Bootcamp = require('./models/bootcamp.mongo');
const Course = require('./models/course.mongo');
const User = require('./models/user.mongo');
const Review = require('./models/review.mongo');

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

// Read JSON files
const bootcamps = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/bootcamp.json`, 'utf-8')
);

const courses = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/courses.json`, 'utf-8')
);

const user = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/users.json`, 'utf-8')
);

const reviews = JSON.parse(
    fs.readFileSync(`${__dirname}/_data/reviews.json`, 'utf-8')
);

// Import into DB
const importData = async () => {
    try {
        await Bootcamp.create(bootcamps);
        console.log('Bootcamp imported...')

        await Course.create(courses);
        console.log('Course imported...')

        await User.create(user);
        console.log('Users imported...')

        await Review.create(reviews);
        console.log('Review imported...')

        process.exit()
    } catch (e) {
        console.log(e)
    }
};

// Delete data
const deleteData = async () => {
    try {
        await Bootcamp.deleteMany();
        console.log('Bootcamp data deleted...')

        await Course.deleteMany();
        console.log('Course data deleted...')

        await User.deleteMany();
        console.log('Users data deleted...')

        await Review.deleteMany();
        console.log('Review data deleted...')

        process.exit()
    } catch (e) {
        console.log(e)
    }
};

if (process.argv[2] === '-i') {
    importData();
}
else if (process.argv[2] === '-d') {
    deleteData();
}
else {
    console.log('Provide some arg\n "-i" -- to import data,\n "-d -- to delete data');
    process.exit();
}