const express = require('express');
const { getCourses, getCourse, addCourse, updCourse, deleteCourse } = require('../controllers/courses.controllers')

const advancedResults = require('../middleware/advancedResults');
const Course = require('../models/course.mongo');

const router = express.Router({ mergeParams: true });

const { protect, authorize } = require('../middleware/auth.middleware');

router
    .route('/')
    .get(advancedResults(Course, { path: 'bootcamp', select: 'name description'}), getCourses)
    .post(protect, authorize('publisher', 'admin'), addCourse);


// router
//     .route('/:bootcampId/courses')
//     .get(getCourses);

router
    .route('/:id')
    .get(getCourse)
    .put(protect, authorize('publisher', 'admin'), updCourse)
    .delete(protect, authorize('publisher', 'admin'), deleteCourse);



module.exports = router;