const express = require('express');
const { register,
    login,
    getMe,
    updateUser,
    forgotPassword,
    resetPassword,
    updateDetails,
    updatePassword,
    getLogout
} = require('../controllers/auth.controllers');
const { protect } = require("../middleware/auth.middleware");


const router = express.Router();

router
    .post('/register', register)
    .post('/login', login)
    .get('/me', protect, getMe)
    .get('/logout', protect, getLogout)
    .post('/forgotpassword', forgotPassword)
    .put('/updatedetails', protect, updateDetails)
    .put('/updatepassword', protect, updatePassword);

router.put('/resetpassword/:resettoken', resetPassword);

router.put('/update', protect, updateUser);

module.exports = router;