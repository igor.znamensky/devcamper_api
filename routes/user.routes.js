const {
    getAllUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
} = require('../controllers/users.controllers');

const User = require('../models/user.mongo');
const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require("../middleware/auth.middleware");

const express = require('express');
const router = express.Router();

// Anything below (router) this will use -protect- first
// All routes will be protected and authorized only for admin
router.use(protect);
router.use(authorize('admin'));

router
    .route('/')
        .get(advancedResults(User), getAllUsers)
        .post(createUser);

router
    .route('/:id')
        .get(getUser)
        .put(updateUser)
        .delete(deleteUser);



module.exports = router;
