const express = require('express');
const router = express.Router({mergeParams: true});
const Reviews = require('../models/review.mongo');
const { protect, authorize } = require('../middleware/auth.middleware');

const {
    getReviews,
    getSingleReview,
    addReview,
    updReview,
    deleteReview
} = require('../controllers/review.controllers');
const advancedResults = require("../middleware/advancedResults");


router
    .route('/')
        .get(advancedResults(Reviews, {
            path: 'bootcamp',
            select: 'name description'
        }), getReviews)
        .post(protect, authorize('user', 'admin'), addReview)

router
    .route('/:id')
        .get(getSingleReview)
        .put(protect, authorize('user', 'admin'), updReview)
        .delete(protect, authorize('user', 'admin'), deleteReview);

module.exports = router;