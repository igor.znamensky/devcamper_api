const express = require('express');

const advancedResults = require('../middleware/advancedResults');
const Bootcamp = require('../models/bootcamp.mongo');

const {
        getBootcamps,
        getBootcamp,
        createBootcamp,
        updateBootcamp,
        deleteBootcamp,
        getBootcampInRadius,
        uplPhotoBootcamp
} = require('../controllers/bootcamp.controllers')

// Include other resource routers
const courseRouter = require('./courses.routes');
const reviewRouter = require('./reviews.routes');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth.middleware');

// Re-route into other resource routers
router.use('/:bootcampId/courses', courseRouter);
router.use('/:bootcampId/reviews', reviewRouter);

router
    .route('/radius/:zipcode/:distance')
    .get(getBootcampInRadius);

router
    .route('/')
    .get(advancedResults(Bootcamp, 'courses'), getBootcamps)
    .post(protect, createBootcamp);

router
    .route('/:id')
    .get(getBootcamp)
    .put(protect, authorize('publisher', 'admin'), updateBootcamp)
    .delete(protect, authorize('publisher', 'admin'), deleteBootcamp)

router
    .route('/:id/photo')
    .put(protect, authorize('publisher', 'admin'), uplPhotoBootcamp)

module.exports = router;
