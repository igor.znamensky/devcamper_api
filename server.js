const express = require('express');

/* Security*/
const helmet = require("helmet");
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const mongoSanitize = require('express-mongo-sanitize');
/* -- */

const path = require('path');
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');
const morgan = require('morgan');
const errorHandler = require('./middleware/error')
const connectDB = require('./config/db');

// Load env vars (FIRST!)
dotenv.config({ path: './config/config.env' });


// Routers file
const bootcamps = require('./routes/bootcamps.routes');
const courses = require('./routes/courses.routes');
const auth = require('./routes/auth.routes');
const users = require('./routes/user.routes');
const reviews = require('./routes/reviews.routes');


// Connect to DB
connectDB();

const app = express();
const PORT = process.env.PORT || 5000;

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser())


// Logging middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('tiny'));
}

// File upload
app.use(express.static(path.join(__dirname, 'pub')));

// File uploading
app.use(fileupload());

/* Security */
// Sanitizes user-supplied data to prevent MongoDB Operator Injection
app.use(mongoSanitize());

// Secure your Express
app.use(helmet());

// Sanitize user input coming from POST body, GET queries, and url params
app.use(xss());

// Basic rate-limiting middleware for Express
const apiLimiter = rateLimit({
	windowMs: 10 * 60 * 1000, // 15 minutes
	max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
})
app.use(apiLimiter);

// To protect against HTTP Parameter Pollution attacks
app.use(hpp());

/* --- */

// Mount routers
app.use('/api/v1/bootcamps', bootcamps);
app.use('/api/v1/courses', courses);
app.use('/api/v1/auth', auth);
app.use('/api/v1/users', users);
app.use('/api/v1/reviews', reviews);

// Error handler
app.use(errorHandler);

// Start server
const server = app.listen(
    PORT,
    console.log(`Server running in ${PORT}`)
);

process.on('unhandledRejection', (err, promose) => {
    console.log(`Err: ${err.message}`)
    // Exit
    server.close( () => process.exit(1))
});